# Backstage
Code Challenge - Underwater Basket Weaving

## Contents of This Repository

* Weather-Sales Correlation Presentation.pptx - A presentation summarizing the approach with an illustration
of a sample chart that can be generated
* create_backstage_tables.sql - A MySQL script to create the tables used (does not include the existing Chinook tables)
* weather.py - A python module to obtain the weather data and populate the warehouse tables. This is not
complete - some functionality is indicated only in the comments. The weather API initially selected is
not accepting new registrations this week due to a system upgrade. The API implemented here is more limited -
the free version only provides data from the US and Canada, and only accepts postal codes as locators. 
Unimplemented functionality and areas were code should be made more efficient and have better error handling
are discussed in the comments within this module.
* sample_plot.R - a tiny R program that generated the bar plot shown in the powerpoint.
 
## Other Requirements

* Must have pymysql installed and a local MySQL installation containing the Chinook database. The process is
fairly database agnostic and should port to other platforms without much difficulty. MySQL was chosen
because I have a local MySQL instance.
* The full implementaiton as intended would require geocoder, but that has been commented out since it is not
being used
* An account at Weathersource is needed. The API key hard-coded in will work for limited evaluation.

## The Data

* The Chinook data was downloaded from https://chinookdatabase.codeplex.com/
* Additional tables required by the process are created by running the create_backstage_tables.sql script
* For purposes of this exercise the invoice date is assumed to be the same as the date of the sale and
the billing address is used to represent the customer's location for purposes of determining their weather (we
don't really know the actual customer location on that day, and we do not have other means, such as an IP 
address of better locating them).
* Sales amount is assumed to always be in US Dollars

### Weather Data
There are numerous sources for historical weather data. 
We can expect to be able to obtain, at minimum, high and low temperatures, precipitation, 
and some indication of general conditions.
As our customer base is international, we need to consider global sources. The free API used for this exercise
supports only US and Canada (the same API can be used globally if upgraded to a paid account).
The weather description is a field not available in the free Weathersource API we are using. 
World Weather Online (which has taken their developer site temporarily down for maintenance this week)
does has this very useful field available (it's existence was assumed in the sample plot in the Powerpoint).
We can develop some rules for deriving a description from the numeric data and including that in the weather table.

For this exercise we are storing the raw weather measurement as numbers.
It may be more enlightening to partition some into ranges standardized for common reporting requirements.
For example, the humidity may be split into ranges, e.g., 0-29, 30-49, 50-74, 75-89, 90-94, 95-100.
This would be implemented by adding a humidity_range dimension table, and including the id for that
dimension in the weather dimension.


## Strategy and Assumptions

* We are creating a very limited DataMart useful only for analyzing weather and sales. Normally, other dimensions
would be included.
* Daily processing of the transactional sales data is assumed. If the analytics need is for something closer to near
real-time the processing would be slightly different - however this could be problematic to implement in the
absence of a true timestamp on the invoice.
* It has been assumed that existing analytical tools are in place to present the data graphically.
* As mentioned above, in the absence of any record of the actual weather experienced by the customer, 
we have assumed the user's location, as inferred from their billing address can be used to determine the
weather experienced by the user on the day of the sale. The billing address was selected over the customer
address as it is more likely to be kept current by the user. Since our product is related to Underwater Basket
Weaving rather than the downloads contained in the Chinook database, this might not be a valid assumption, but our
database does not contain shipping addresses. 
* The possibility of using machine learning to discover more complex relationships between multiple
weather factors and sales has been considered, and deferred to a future exercise. All the factors other than 
the overall weather description are numeric and should lend themselves to study by regression methods.

## Testing

The weather API has been tested for the first few rows retrieved for a few dates specified in 2009, allowing
testing of the generation of the weather and date dimension data.
The full implementation of the generation and storage of the fact table remains to be completed, so testing
of the end result remains to be done.

## Reporting
The star schema used here will lend itself readily to analysis by standard BI tools. If there will be a 
frequent need to aggregate data based on calculated quantities, summary tables can be added to 
hold these quantities for rapid retrieval without adding
significantly to the time needed to store the daily data. An example of this would be a calculated
discomfort index.

## Scalability
The ultimate size of the fact table is determined by the number of distinct locations in which we record sales
and the number of distinct dates those sales occur. If we make the very optimistic assumption that we have
sales in 50,000 distinct locations every day of the year, and we hold 10 years of data, we are dealing with
under 200,000,000 rows. That is not an excessive amount. If older data is less commonly used, performance gains
can be had by partitioning based on the year. The weather measurements have a relatively small number of distinct
values, so given that a the type of queries expected a columnar database will have advantages.

