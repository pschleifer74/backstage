from traceback import format_exc
from datetime import date, datetime
#import geocoder - Not geocoding for prototype
import pymysql.cursors
import requests
import json
from blaze.tests.test_sql import sql

worldWeatherOnlineBaseUrl = "http://api.worldweatheronline.com/free/v2/past-weather.ashx"

#TODO: Put this in a config file
weatherSourceAPIKey = "e5831ff9b25f0c239f7f"

weatherAPI = { "provider" : "WeatherSource",
               "BaseURL" : "https://api.weathersource.com/v1/",
               "APIKey": weatherSourceAPIKey }

weatherURL = weatherAPI["BaseURL"] + weatherAPI["APIKey"] + "/history_by_postal_code.json"
# This parameter is unvarying
weatherParams = { "fields": "tempMax,tempAvg,tempMin,precip,snowfall,windSpdMax,relHumAvg" }

#TODO: Get the password out of here and create a new user so I don't have to use root
connection = pymysql.connect(host='localhost',
                             user='root',
                             password='lelaetl',
                             db='chinook',
                             cursorclass=pymysql.cursors.DictCursor)

weatherCursor = connection.cursor()

def getWeather( theDate, city=None, state=None, country=None, postal=None ):
    """ Returns the weather ID - either the cached or newly fetched
        Check for existing cached data - key is ordinal date, latitude, longitude 
        For prototype, can only use postal codes, so key is ordinal date, postal code
        
        Since we are running this process for a single date at a time, consider
        caching the weather data for that date in memory, and writing out to database
        at the end of the processing for the day
    """
    
    # For WeatherSource trial, can only do US/Canada
    if country not in ["USA", "Canada"] or postal is None:
        return None
    
    ordinalDate =  theDate.toordinal()
    sql = "SELECT WeatherId FROM weather WHERE WeatherDateOrdinal = %s AND PostalCode = %s"
    weatherCursor.execute(sql, (ordinalDate, postal))
    result = weatherCursor.fetchone()
    if result is not None:
        return result["WeatherId"]
# https://api.weathersource.com/v1/e5831ff9b25f0c239f7f/history_by_postal_code.json?postal_code_eq=11790&country_eq=US&timestamp_eq=2009-01-15&fields=tempMax,tempAvg,tempMin

    weatherParams["postal_code_eq"] = postal
    # Need the ISO code here, so use this ugly code because there are only 2 possibilities
    weatherParams["country_eq"] = "US" if country == "USA" else "CA"
    # Date passes as yyyy-mm-dd
    weatherParams["timestamp_eq"] = theDate.strftime("%Y-%m-%d")
    print weatherParams
    
    result = requests.get(weatherURL, params=weatherParams)
    try:
        response = json.loads(result.content)
        #print response
        if len(response) == 0:
            print 'No JSON in response'
            return None
    except:
        print 'Could not parse json for details in', result.content
        return None
    
    print response
    sql = """INSERT INTO weather (
        WeatherDateOrdinal, 
        PostalCode, 
        MinTempF, 
        MaxTempF,
        HumidityPct,
        MaxWindMPH,
        Precipitation,
        Snowfall )
        VALUES ( %s, %s, %s, %s, %s, %s, %s, %s )"""
    
    #TODO: Derive a weather description from the numeric data (this is unfortunately not provided
    # by WeatherSource (some other API's do have this), but will be very useful for running some
    # quick analyses
    
    # Make sure the ones which are defined as integers in the db get passed that way
    # We are not looking for excessive precision
    try:
        weatherCursor.execute(sql, (ordinalDate, 
                                postal, 
                                int(response[0].get("tempMin")),
                                int(response[0].get("tempMax")),
                                int(response[0].get("relHumAvg")),
                                int(response[0].get("windSpdMax")),
                                response[0].get("precip"),
                                response[0].get("snowfall") ))
    except:
        print "Failed:", sql
        print format_exc()
    else:
        sql = "SELECT @@last_insert_id AS last_insert_id"
        weatherCursor.execute(sql)
        new_id = weatherCursor.fetchone()
        connection.commit()
        return new_id.get("last_insert_id")
        
def doInvoices (theDate, limit = 10):
    """ This will accumulate the sales totals for each weather location for the given date
        In ongoing use this process will run daily to accumulate the entries for the given date
        For initial population of the database this will need to run once for each date in the existing data
        
        This will build a list in memory of the accumulates sales totals for each unique weather location,
        then write these out to the sales table
        The unique weather location data will be written out as it is encountered
    """

    sql = "SELECT SaleDateId FROM sale_date WHERE DateDay = %s AND DateMonth = %s AND DateYear = %s"
    try:
        dt = datetime.strptime(theDate,"%Y-%m-%d")
        myCursor = connection.cursor()
        myCursor.execute(sql, (dt.day, dt.month, dt.year))
        result = myCursor.fetchone()
        if result is None:
            # TODO: set the Quarter
            sql = """ INSERT INTO sale_date (DateDay, DateMonth, DateYear, DateQuarter)
                      VALUES (%s, %s, %s, 0) """
            myCursor.execute(sql, (dt.day, dt.month, dt.year))
            sql = "SELECT @@last_insert_id AS last_insert_id"
            myCursor.execute(sql)
            dateId = myCursor.fetchone()["last_insert_id"]
        else:
            dateId = result["SaleDateId"]
        print "dateId", dateId
    except:
        print "Failed to read or add sale_date table"
        print format_exc()
        return
    
    salesTotals = []
    
    try:
        # For prototype, restrict to USA/Canada due to free API restriction
        sql = "SELECT InvoiceId, InvoiceDate, BillingCity, BillingState, BillingCountry, BillingPostalCode " +\
            " FROM invoice " +\
            " WHERE InvoiceDate = %s " +\
            " AND BillingCountry IN ('USA','Canada') "
        myCursor.execute(sql, (theDate))
        rowsRemaining = limit
        while rowsRemaining > 0:
            result = myCursor.fetchone()
            rowsRemaining -= 1
            if result is None:
                break
            
            print result
            #TODO: Convert zip+4 to plain zip
            weatherId = getWeather(result["InvoiceDate"], 
                       result["BillingCity"], 
                       result["BillingState"], 
                       result["BillingCountry"], 
                       result["BillingPostalCode"])
            print "weatherId", weatherId
            
            #TODO: Implement handling for when weather could not be found (weatherId is None)
            """ Failure to obtain a WeatherId could be do to any of the following:
                    Missing or invalid postal code
                    API could not return a row because no data exists on that date
                    Weather API provider experiencing temporary difficulties
                If we are getting too many errors not due to missing postal codes we want to abort the process
            """
            
            #TODO: Accumulate the totals for this weatherId/Date
            
            #TODO: Insert all the rows for this date into the sales table
    except:
       print format_exc()
        

    
    
def doDates(fromDate, toDate):
    """ This will accumulate the sales totals for each weather location for the given date
        In ongoing use this process will run daily to accumulate the entries for the given date
        For initial population of the database this will need to run once for each date in the existing data
        
        To be implemeneted 
    """
    pass

# Try it with one date
doInvoices(theDate='2009-01-06',limit=25)
connection.commit()
connection.close()